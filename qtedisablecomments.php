<?php
/*
 * Plugin Name: QTE Disable Comments
 * Description: QTE Disable Comments disables comments for all post types
 * Version: 0.1.0
 * Author: QTE Development AB
 * Author URI: https://getqte.se/
 */

 if( ! defined('ABSPATH')) {
     exit;
 }

 define('QTEDISABLECOMMENTS_VERSION', '0.1.0');

define( 'QTEDISABLECOMMENTS_PLUGIN_DIR', plugin_dir_url( __FILE__ ) );
define( 'QTEDISABLECOMMENTS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );


require_once QTEDISABLECOMMENTS_PLUGIN_PATH . 'includes/disable_comments.php';
